package com.limao.dao;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.limao.utils.DateTools;

@Component(value="TransLogDaoImpl")
public class TransLogDaoImpl implements TransLogDao {
	
	@Autowired
	private DateTools dateSwitch;
	
	@Resource(name="jdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	
	public void insert(Object input) {
		String sql = "insert into TransLog (content,date,user)values(?,?,?)";
		jdbcTemplate.update(sql, "insert "+input.toString(),dateSwitch.DateTodayWithHMS(),"��ï");
	}

}
