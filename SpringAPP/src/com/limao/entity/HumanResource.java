package com.limao.entity;

import org.springframework.stereotype.Component;

@Component(value = "HumanResource")
public class HumanResource {
	private Integer id;
	private String name;
	private String sex;
	private Integer age;
	private String PhoneNumber;
	private String birthday;

	public HumanResource() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HumanResource(Integer id, String name, String sex, Integer age, String phoneNumber, String birthday) {
		super();
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.age = age;
		PhoneNumber = phoneNumber;
		this.birthday = birthday;
	}

	public HumanResource(String name, String sex, String phoneNumber, String birthday) {
		super();
		this.name = name;
		this.sex = sex;
		this.PhoneNumber = phoneNumber;
		this.birthday = birthday;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	@Override
	public String toString() {
		return "HumanResource [name=" + name + ", sex=" + sex + ", age=" + age + ", PhoneNumber="
				+ PhoneNumber + "]";
	}	

}
