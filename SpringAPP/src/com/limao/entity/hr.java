package com.limao.entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="hr")
public class hr {
	private String Username;
	@Value("123456")
	private String Password;
	
	public hr() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	@Override
	public String toString() {
		return "hr [Username=" + Username + ", Password=" + Password + "]";
	}

}
