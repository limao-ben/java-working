package com.limao.service;

import java.util.List;

import com.limao.entity.HumanResource;

public interface HumanResourceService {

	public void save(HumanResource hr);

	public List<HumanResource> queryall();

}
