package com.limao.service;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component(value="HumanResourceAop")
@Aspect
public class HumanResourceAop {
	    /**
	     * 前置通知
	     */
	    @Before("execution(* com.limao.service.HumanResourceServiceImpl.save(..))")
	    public void before(){
	        System.out.println("前置通知....");
	    }

	    /**
	     * 后置通知
	     * returnVal,切点方法执行后的返回值
	     */
	    @AfterReturning(value="execution(* com.limao.service.HumanResourceServiceImpl.save(..))",returning = "returnVal")
	    public void AfterReturning(Object returnVal){
	        System.out.println("后置通知...."+returnVal);
	    }
	}
