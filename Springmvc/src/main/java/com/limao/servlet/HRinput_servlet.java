package com.limao.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.limao.entity.HumanResource;
import com.limao.service.*;

/**
 * Servlet implementation class InputHumanResource
 */
@WebServlet("/HRinput")
public class HRinput_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HRinput_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HumanResourceService hr = new HumanResourceServiceImpl();
		doGet(request, response);
		String name =request.getParameter("name");
		//System.out.print(name+"\n");
		String sex = request.getParameter("sex");
		//String age = request.getParameter("age");
		String PhoneNumber = request.getParameter("tel");
		String birthday = request.getParameter("year")+"-"+request.getParameter("month")+"-"+request.getParameter("day");
		HumanResource HRinput =new HumanResource(name,sex,PhoneNumber,birthday);
		//System.out.print(name+"\n");
		hr.save(HRinput);
		}
	}

