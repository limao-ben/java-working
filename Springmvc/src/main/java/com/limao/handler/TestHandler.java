package com.limao.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/test")
public class TestHandler {
	   @RequestMapping(value="/test1")
	    public String login(){
	        
	        System.out.println("用户登录");
	        
	        return "login";
	                
	    }
	   @RequestMapping(value="/*/login")
	   public String login2() {
		   System.out.println("user login\n");
		   return "login";
	   }
	   @RequestMapping(value="/testparam")//,params={"id=1"}
	    public String login3(@RequestParam(defaultValue="1") int id){
	        
	        System.out.println("用户登录");
	        System.out.println(id);
	        
	        return "login";
	                
	    }/*
	   @RequestMapping(value="/abc")
	   public String login4() {
		   System.out.println("abc");
		   return "login";
	   }*/
	   
	    @RequestMapping(value="/abc")
	    @ResponseBody
	    public ModelAndView index(@RequestParam String fname,@RequestParam String lname){  
	        ModelAndView modelAndView = new ModelAndView("login"); 
	        modelAndView.addObject("name", fname); 
	        modelAndView.addObject("key1", lname); 
	        return modelAndView;  
	    } 
	    
	   /* @RequestMapping(value="/map123")  
	    public String index(Model model) {  
	        String retVal = "login";  
	        //User user = new User();  
	        //user.setName("limao");  
	        model.addAttribute("key1", "李茂");
	        return retVal;  
	    }  */
	    
	    @RequestMapping(value="/map123")
	    @ResponseBody
	    public String index2() {  
	    	Map<String, String> map = new HashMap<String, String>();  
	        map.put("key1", "limao");  
	        //map.put相当于request.setAttribute方法  
	        return "login"; 
	    }
	    @RequestMapping(value="/input")
	    public String input() {
	    	return "input";
	    }
}
