package com.limao.handler;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.limao.dao.UserDao;
import com.limao.entity.User;

@Controller
public class UserHandler {
	
	@Resource(name="UserDaoImpl")
	private UserDao userdao;
	
	//增加操作，是void，跳转到input界面，没有内容，没有id
	@RequestMapping(value = "/user"/*, method = RequestMethod.POST*/)
	@ResponseBody
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("add"); 
		int newid = userdao.SearchMaxId()+1;
		modelAndView.addObject("newid", newid);
		System.out.println("use save method:"+newid+"\n");
		return modelAndView;
	}
	
	//删除操作,是void
	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ModelAndView delete(@PathVariable("id") Integer id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/user/all"); 
		userdao.delete(id);
		System.out.println("use delete method and delete id="+id+"\n");
		return modelAndView;
	}
	
	//全部查询，userdao返回的是一个json
	@RequestMapping("/userall")
	@ResponseBody
	public ModelAndView list(Map<String, Object> map) {
		ModelAndView modelAndView = new ModelAndView("show"); 
		modelAndView.addObject("user", userdao.SelectAlljson());
		System.out.println("search all of data\n");
		return modelAndView;//对应到了show.jsp
	}
	
	//改动操作,跳转到input界面，并保留id（显示内容，不显示id）
	@RequestMapping(value = "/user/{id}"/*, method = RequestMethod.PUT*/)
	@ResponseBody
	public ModelAndView update(@PathVariable("id") Integer id) {
		ModelAndView modelAndView = new ModelAndView("update");
		modelAndView.addObject("user",userdao.SelectByID(id));
		modelAndView.addObject("newid", id);
		System.out.println("update "+id+"\n");
        return modelAndView;
	}
	
	//在改变界面的特别处理
	@RequestMapping(value="/user/update")
	@ResponseBody
	public ModelAndView update(User user) {
		ModelAndView modelAndView = new ModelAndView("show");
		userdao.update(user);
		return modelAndView;
	}
	
	//在新增界面的特别处理
	@RequestMapping(value="/user/add")
	@ResponseBody
	public ModelAndView add(User user) {
		ModelAndView modelAndView = new ModelAndView("show");
		userdao.insert(user);
		return modelAndView;
	}
	

}
