package com.limao.utils;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.*;
import com.limao.entity.User;

@Component(value ="JsonTranslate")
public class JsonTranslate {
	
	public JSONArray ListUserToJson(List<User> list) {
		String jsonString = JSON.toJSONString(list);
		JSONArray jsonArray = JSONArray.parseArray(jsonString);
		return jsonArray;
	}

}
