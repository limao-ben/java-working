package com.limao.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.limao.entity.User;
import com.limao.utils.JsonTranslate;

@Component(value ="UserDaoImpl")
public class UserDaoImpl implements UserDao {
	
	@Resource(name="jdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Resource(name="JsonTranslate")
	private JsonTranslate jsontranslate;
	
	//增
	@Override
	public void insert(User user) {
		String sql ="insert into User (Username,Password,Place,Sex)values(?,?,?,?)";
        //List<HumanResource> hrlist = jdbc.query(sql, rowMapper, 1); 
        jdbcTemplate.update(sql, user.getUsername(),user.getPassword(),user.getPlace(),user.getSex());
		
	}
	
	//查（全部）
	@Override
	public JSONArray SelectAlljson() {
		// TODO Auto-generated method stub
		String sql = "select * from User";
		RowMapper<User> rowMapper = new BeanPropertyRowMapper<>(User.class);
		List<User> list = jdbcTemplate.query(sql, rowMapper);
		JSONArray jsonarray = jsontranslate.ListUserToJson(list);
		return jsonarray;
	}
	
	@Override
	public List<User> SelectAll() {
		// TODO Auto-generated method stub
		String sql = "select * from User";
		RowMapper<User> rowMapper = new BeanPropertyRowMapper<>(User.class);
		List<User> list = jdbcTemplate.query(sql, rowMapper);
		return list;
	}
		
	//查（部分）
	@Override
	public List<User> SelectByID(int id) {
		// TODO Auto-generated method stub
		String sql = "select * from User where id = ?";
		RowMapper<User> rowMapper = new BeanPropertyRowMapper<>(User.class);
		List<User> list = jdbcTemplate.query(sql, rowMapper,id);
		return list;
	}
	
	//删
	@Override
	public void delete(int id) {
		String sql ="delete from User where id = ?";
		jdbcTemplate.update(sql,id);
	}
	
	//改
	@Override
	public void update(User user) {
		String sql = "update User set Username =?,Password=?,Place=?,Sex=? where id =?";
		jdbcTemplate.update(sql,user.getUsername(),user.getPassword(),user.getPlace(),user.getSex(),user.getId());
	}
	
	@Override
	public int SearchMaxId() {
		String sql = "select max(id) from user ";
		RowMapper<User> rowMapper = new BeanPropertyRowMapper<>(User.class);
		List<User> list = jdbcTemplate.query(sql, rowMapper);
		System.out.println(list);
		User user = list.get(0);
		System.out.println(user);
		int value =user.getId();
		return value;
	}

}
