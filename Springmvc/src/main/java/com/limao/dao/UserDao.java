package com.limao.dao;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.limao.entity.User;

public interface UserDao {
	void insert(User user);
	JSONArray SelectAlljson();
	List<User> SelectAll();
	List<User> SelectByID(int id);
	void delete(int id);
	void update(User user);
	int SearchMaxId();

}
