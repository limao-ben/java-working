package com.limao.dao;

import java.util.List;

import com.limao.entity.HumanResource;

public interface HRinputDao {
	void insert(HumanResource hr);
	List<HumanResource> SelectAll();
	List<HumanResource> SelectByID(int id);
}
