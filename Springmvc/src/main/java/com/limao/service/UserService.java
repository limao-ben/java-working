package com.limao.service;

import java.util.List;

import com.limao.entity.User;

public interface UserService {
	public void save(User user);

	public List<User> queryall();

	boolean valid(String username, String password);

}
