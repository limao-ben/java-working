package com.limao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.limao.dao.TransLogDao;
import com.limao.entity.HumanResource;
import com.limao.dao.HRinputDao;

@Component(value="HumanResourceServiceImpl")
public class HumanResourceServiceImpl implements HumanResourceService {
	
	@Resource(name="HRinputDaoImpl")
	private HRinputDao hrinputDao;
	
	@Resource(name="TransLogDaoImpl")
	private TransLogDao TransLogDao;
	
	@Override
	@Transactional
	public void save(HumanResource hr) {
		hrinputDao.insert(hr);
		TransLogDao.insert(hr);
		System.out.print("success\n");
		
	}
	
	@Override
	public List<HumanResource> queryall() {
		List<HumanResource> query=hrinputDao.SelectAll();
		System.out.print("success\n");
		return query;
	}

}
