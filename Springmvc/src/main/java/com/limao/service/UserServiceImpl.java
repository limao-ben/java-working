package com.limao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.limao.dao.TransLogDao;
import com.limao.dao.UserDao;
import com.limao.entity.User;


@Component(value = "UserServiceImpl")
public class UserServiceImpl implements UserService {
	
	@Resource(name="UserDaoImpl")
	private UserDao userDao;
	
	@Resource(name="TransLogDaoImpl")
	private TransLogDao TransLogDao;

	@Override
	@Transactional
	public void save(User user) {
		// TODO Auto-generated method stub
		userDao.insert(user);
		TransLogDao.insert(userDao);
		System.out.print("success\n");
		
	}

	@Override
	public List<User> queryall() {
		// TODO Auto-generated method stub
		List<User> query=userDao.SelectAll();
		System.out.print("success\n");
		return query;
	}
	
	@Override
	public boolean valid(String username,String password) {
		return false;
		//
	}

}
