package com.limao.service;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component(value="HumanResourceAop")
@Aspect
public class HumanResourceAop {
	    /**
	     * ֪
	     */
	    @Before("execution(* com.limao.service.HumanResourceServiceImpl.save(..))")
	    public void before(){
	        System.out.println("成功");
	    }

	    /* ֪
	     * ֵ
	     */
	    @AfterReturning(value="execution(* com.limao.service.HumanResourceServiceImpl.save(..))",returning = "returnVal")
	    public void AfterReturning(Object returnVal){
	        System.out.println("success"+returnVal);
	    }
	}
