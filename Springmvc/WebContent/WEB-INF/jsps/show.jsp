<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Input</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script type="text/javascript">
$(function() {  
    //alert("hello");  
    $(".delete").click(function(){
        var href = $(this).attr("href");
        $("form").attr("action",href).submit();
        return false;
        })
}); 
</script>
</head>
<body>
<!-- 这里使用的是一般的form，并没有去使用mvc form  如果用mvc form会在回调的时候有一些优势 -->
    <form action="" method="post">
        <input type="hidden" name="_method" value="DELETE"/>
    </form>

    <c:if test="${empty requestScope.user}">
        没有任何用户信息。
    </c:if>
    
    <c:if test="${!empty requestScope.user}">
    <table border="1" >
            <tr>
                <td>ID</td>
                <td>Username</td>
                <td>Password</td>
                <td>Place</td>
                <td>Sex</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            
            <c:forEach items="${user}" var="user">
                <tr>
                    <th>${user.id}</th>
                    <th>${user.Username}</th>
                    <th>${user.Password}</th>
                    <th>${user.Place}</th>
                    <th>${user.Sex}</th>
                    <th><a href="user/${user.id}">Edit</a></th>
                    <th><a class="delete" href="user/${user.id}">Delete</a></th>

                </tr>
            </c:forEach>

        </table>

    
    </c:if>
    
    
    <br><br>
    <a href="user">Add New User</a>


</body>
</html>