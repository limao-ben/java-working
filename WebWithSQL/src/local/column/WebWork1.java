package local.column;

public class WebWork1 {
	private int id;
	private String Username;
	private String Password;
	private String Place;
	private String Sex;
	
	public WebWork1(String username, String password, String place, String sex) {
		super();
		Username = username;
		Password = password;
		Place = place;
		Sex = sex;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getPlace() {
		return Place;
	}
	public void setPlace(String place) {
		Place = place;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	@Override
	public String toString() {
		return "WebWork1 [Username=" + Username + ", Password=" + Password + ", Place=" + Place
				+ ", Sex=" + Sex + "]";
	}

}
