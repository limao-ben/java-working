package local.column;

import java.sql.Timestamp;

public class DatabaseLog {
	private int id;
	private String content;
	private Timestamp date;
	private String user;

//���캯��
	public DatabaseLog(int id, String content, Timestamp date, String user) {
		super();
		this.id = id;
		this.content = content;
		this.date = date;
		this.user = user;
	}
	
//set&get
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

//toString	
	@Override
	public String toString() {
		return "DatabaseLog [content=" + content + ", date=" + date + ", user=" + user + "]";
	}

}
