package local.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import local.column.HumanResource;
import local.tool.MysqlManager;

public class HRinputDAO implements I_HRinputDAO {
	@Override
	public boolean input(HumanResource hr) {
		boolean result = false;
		Connection m =MysqlManager.getCon();
		String SQLComment = "insert into HumanResource (name,sex,age,PhoneNumber,birthday)values(?,?,?,?,?)";
		try {
			PreparedStatement pst = m.prepareStatement(SQLComment);
			pst.setString(1, hr.getName());
			pst.setString(2, hr.getSex());
			pst.setInt(3, hr.getAge());
			pst.setString(4, hr.getPhoneNumber());
			pst.setString(5, hr.getBirthday());
			pst.executeUpdate();
			result =true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return result;
		
	}
	public List<HumanResource> showdata() {
		List<HumanResource> list =new ArrayList<>();
		Connection m =MysqlManager.getCon();
		String SQLComment = "select * from HumanResource";
		try {
			PreparedStatement pst = m.prepareStatement(SQLComment);
			ResultSet rs = pst.executeQuery();//rs是数据集
    		//获取列数
			//Map<String,int> =new
			while(rs.next()==true) {
	   		  	Integer id=rs.getInt("id");
	   			String name=rs.getString("name");
	   			String sex=rs.getString("sex");
	   			Integer age=rs.getInt("age");
	   			String PhoneNumber=rs.getString("PhoneNumber");
	   			String birthday=rs.getString("birthday");
	   			HumanResource hr =new HumanResource(id,name,sex,age,PhoneNumber,birthday);
	   			list.add(hr);
	   			System.out.print(hr.toString()+"\n");
   		}
    		//System.out.print(SQLComment);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}


}
