package local.DAO;

import local.column.WebWork1;

public interface I_UserDAO {
	boolean valid(String name,String password);
	void add(WebWork1 webwork1);
	boolean search(String username);

}
