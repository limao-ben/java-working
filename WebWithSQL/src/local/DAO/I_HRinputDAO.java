package local.DAO;

import java.util.List;

import local.column.HumanResource;

public interface I_HRinputDAO {
	boolean input(HumanResource hr);
	List<HumanResource> showdata();
}
