package local.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.DAO.I_UserDAO;
import local.DAO.UserDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/admin_Login")
public class Login_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		//System.out.print("YES\n");
		String username =request.getParameter("username");
		String password =request.getParameter("password");
		//System.out.print(username+"\n"+password+"\n");
		//response.getWriter().write("\nusername "+username+"\n"+"password "+password+"\n");
		I_UserDAO user = new UserDAO();
		Boolean success =user.valid(username, password);
		if (success == true) {
			//response.getWriter().write("exist");
			//request.getRequestDispatcher("MainPage.jsp").forward(request, response);
			response.sendRedirect("MainPage.jsp");
		}else {
			response.getWriter().write("not exist");
			System.out.print("\nFalse");
		}
	}

}
