<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.* "%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="name" value="zhangsan" scope="request"></c:set>
	${requestScope.name}

	<%
	class User{
	public String name;
	public String sex;
	//public User(){};
	public User(String setname,String setsex){
		//super();
		this.name=setname;
		this.sex=setsex;
	}
	//public void setName(String input){
	//	this.name=input;
	//}
}
	
	List<User> list = new ArrayList<>();
    User user1 = new User("小绿","女");
    User user2 = new User("小黑","男");
    list.add(user1);
    list.add(user2);
//  java程序中的写法
//    for (int i = 0; i < list.size(); i++) {
//        System.out.println(i);
//    }
//    for (User user : list) {
//        System.out.println(user.getName());
//    }
    request.setAttribute("list",list);//把list放入域对象，后面从集合其中取数据
    
%>
	<c:forEach begin="0" end="${list.size()-1}" var="i">
    ${list[i].name}
	</c:forEach>

	<c:forEach items="${list}" var="user">
    ${user.name}
	</c:forEach>

</body>
</html>