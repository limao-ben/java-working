package com.limao.entity;

import org.springframework.stereotype.Component;

@Component(value="TransLog")
public class TransLog {
	private int id;
	private String content;
	private String date;
	private String user;
	
	public TransLog() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String string) {
		this.date = string;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "DatabaseLog [id=" + id + ", content=" + content + ", date=" + date + ", user=" + user + "]";
	}

}
