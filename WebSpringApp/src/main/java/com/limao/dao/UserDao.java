package com.limao.dao;

import java.util.List;

import com.limao.entity.User;

public interface UserDao {
	void insert(User user);
	List<User> SelectAll();
	List<User> SelectByID(int id);

}
