package com.limao.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.limao.entity.HumanResource;

@Component(value="HRinputDaoImpl")
public class HRinputDaoImpl implements HRinputDao {
	
	@Resource(name="jdbcTemplate")
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public void insert(HumanResource hr) {
		String sql ="insert into HumanResource (name,sex,age,PhoneNumber,birthday)values(?,?,?,?,?)";
        //List<HumanResource> hrlist = jdbc.query(sql, rowMapper, 1); 
        jdbcTemplate.update(sql, hr.getName(),hr.getSex(),hr.getAge(),hr.getPhoneNumber(),hr.getBirthday());				
	}
	
	@Override
	public List<HumanResource> SelectAll() {
		String sql = "select * from HumanResource";
		RowMapper<HumanResource> rowMapper = new BeanPropertyRowMapper<>(HumanResource.class);
		List<HumanResource> list = jdbcTemplate.query(sql, rowMapper);
		return list;
	}
	
	@Override
	public List<HumanResource> SelectByID(int id){
		String sql = "select * from HumanResource where id = ?";
		RowMapper<HumanResource> rowMapper = new BeanPropertyRowMapper<>(HumanResource.class);
		List<HumanResource> list = jdbcTemplate.query(sql, rowMapper,id);
		return list;
	}
}
