package com.limao.test;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.limao.entity.hr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext.xml")
public class sqlTest {
	
	@Resource(name="jdbcTemplate")
	private JdbcTemplate jdbc;
	
	@Test
	public void print() {
		//System.out.println(jdbc);
	}
	
	@Test  
    public void testQueryForList() {  
        String sql = "SELECT * FROM humanresource WHERE id > ?";
        RowMapper<hr> rowMapper = new BeanPropertyRowMapper<>(hr.class);  
        List<hr> hrlist = jdbc.query(sql, rowMapper, 1); 
        System.out.println(hrlist);
	}
}
