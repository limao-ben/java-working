package SQLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class MysqlManager {
	public static Integer FirstCommandResult =0;
	public static Integer SecondCommandResult =0;
    static Connection mConnect;
    //public static int a=1;
    public static Connection getCon() {
    	 try {//先给参数起名字，都是string类型
    		 String driverClass = "com.mysql.cj.jdbc.Driver";//加载数据库驱动
    		 String username = "root";// 用户名
    		 String password = "123456";// 密码
    		 String jdbcUrl = "jdbc:mysql://localhost:3306/test1?useSSL=false&serverTimezone=Asia/Shanghai";
    		 Class.forName(driverClass);//这是为了加载数据库驱动
             mConnect=DriverManager.getConnection(jdbcUrl,username,password);//给对象输入数据
         } catch (ClassNotFoundException | SQLException e) {
             e.printStackTrace();//额外性的报错检查
         }
        return mConnect;
        
    }
    public static void close() {
        try {
            mConnect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //前面函数告一段落，后面的要新增四个函数
    //插入数据    查询数据    更改数据    删除数据 新增四个函数
    
    
    public static void add(String TableName,String name,String sex,String birthday,String PhoneNumber) {
    	try {
    		Connection m =getCon();
    		m.setAutoCommit(false);
    		String SQLComment ="insert into "+TableName+"(name,sex,birthday,age,PhoneNumber)values(?,?,?,?,?)";
    		PreparedStatement pst = m.prepareStatement(SQLComment);
    		pst.setString(1, name);
    		pst.setString(2, sex);
    		pst.setString(3, birthday);
    		//这里调用一个根据时间戳计算age的函数，输出为age(int)
    		Integer age = DateSwitch.CalculateAge(birthday);
    		pst.setInt(4, age);
    		pst.setString(5, PhoneNumber);
    		pst.executeUpdate();
    		System.out.print("add command over\n");
    		//结束后新增
			MysqlManager.FirstCommandResult =1;
			MysqlManager.DatabaseLogInput(SQLComment);
			if(MysqlManager.SecondCommandResult==1) {
				m.commit();
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		MysqlManager.close();
	    		System.out.print("Finish\n");
			}else {
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		System.out.print("执行失败\n");
	    		MysqlManager.close();
			}
    	}catch(SQLException e) {
    		e.printStackTrace();
    		MysqlManager.FirstCommandResult =0;
    		MysqlManager.SecondCommandResult =0;
    		System.out.print("执行失败\n");
    		MysqlManager.close();
    	}
    }
    
    
    public static void add(String TableName,String date,String name,Double price,Double quantity,String buyer) {
    	try {
    		Connection m =getCon();
    		m.setAutoCommit(false);
    		String SQLComment ="insert into "+TableName+"(date,name,price,quantity,buyer,paying)values(?,?,?,?,?,?)";
    		PreparedStatement pst = m.prepareStatement(SQLComment);
    		Timestamp date2 = TimeSwitch.StringToTimestamp(date);
    		pst.setTimestamp(1, date2);
    		pst.setString(2, name);
    		pst.setDouble(3, price);
    		pst.setDouble(4, quantity);
    		pst.setString(5, buyer);
    		Double paying = price* quantity;
    		pst.setDouble(6, paying);
    		pst.executeUpdate();
    		System.out.print("add command over\n");
    		//结束后新增
			MysqlManager.FirstCommandResult =1;
			MysqlManager.DatabaseLogInput(SQLComment);
			if(MysqlManager.SecondCommandResult==1) {
				m.commit();
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		MysqlManager.close();
	    		System.out.print("Finish\n");
			}else {
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		System.out.print("执行失败\n");
	    		MysqlManager.close();
			}
    	}catch(SQLException e) {
    		e.printStackTrace();
    		MysqlManager.FirstCommandResult =0;
    		MysqlManager.SecondCommandResult =0;
    		System.out.print("执行失败\n");
    		MysqlManager.close();
    	}
    }
    
    public static void search(String TableName) {
    	try {
    		Connection m = getCon();
    		m.setAutoCommit(false);
    		String SQLComment = "select * from "+TableName;
    		PreparedStatement pst = m.prepareStatement(SQLComment);
    		ResultSet rs = pst.executeQuery();//rs是数据集
    		//获取列数
    		ResultSetMetaData md= rs.getMetaData();
    		int columnSize = md.getColumnCount();
    		
    		System.out.println("查询结果如下:\n");
    		//打印字段名
    		for(int i = 1; i <= columnSize; i++){
    		    System.out.printf("%-12s",md.getColumnName(i));
    		}
    		System.out.println();
    		//打印所有记录
    		while(rs.next()) {
    		     for(int i = 1; i <= columnSize ; i++){
    		         System.out.printf("%-12s",rs.getObject(i));
    		     }
    		     System.out.println();
    		}

    		System.out.print("search command over\n");
    		//结束后新增
			MysqlManager.FirstCommandResult =1;
			MysqlManager.DatabaseLogInput(SQLComment);
			if(MysqlManager.SecondCommandResult==1) {
				m.commit();
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		MysqlManager.close();
	    		System.out.print("Finish\n");
			}else {
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		System.out.print("执行失败\n");
	    		MysqlManager.close();
			}
    	}catch(SQLException e) {
    		e.printStackTrace();
    		MysqlManager.FirstCommandResult =0;
    		MysqlManager.SecondCommandResult =0;
    		System.out.print("执行失败\n");
    		MysqlManager.close();
    	}
    }
    
    public static void search(String TableName,Integer id) {
    	try {
    		Connection m = getCon();
    		m.setAutoCommit(false);
    		String SQLComment ="select * from "+TableName+" where id = "+id;
    		PreparedStatement pst = m.prepareStatement(SQLComment);
    		ResultSet rs = pst.executeQuery();//rs是数据集
    		//获取列数
    		ResultSetMetaData md= rs.getMetaData();
    		int columnSize = md.getColumnCount();
    		
    		System.out.println("查询结果如下:\n");
    		//打印字段名
    		for(int i = 1; i <= columnSize; i++){
    		    System.out.printf("%-12s",md.getColumnName(i));
    		}
    		System.out.println();
    		//打印所有记录
    		while(rs.next()) {
    		     for(int i = 1; i <= columnSize ; i++){
    		         System.out.printf("%-12s",rs.getObject(i));
    		     }
    		     System.out.println();
    		}
    		System.out.print("search command over\n");
    		//结束后新增
			MysqlManager.FirstCommandResult =1;
			MysqlManager.DatabaseLogInput(SQLComment);
			if(MysqlManager.SecondCommandResult==1) {
				m.commit();
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		MysqlManager.close();
	    		System.out.print("Finish\n");
			}else {
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		System.out.print("执行失败\n");
	    		MysqlManager.close();
			}
    	}catch(SQLException e) {
    		e.printStackTrace();
    		MysqlManager.FirstCommandResult =0;
    		MysqlManager.SecondCommandResult =0;
    		System.out.print("执行失败\n");
    		MysqlManager.close();
    	}
    }
    
    
	public static void update(String TableName,String TargetColumn,Integer TargetId,String NewValue){
		Connection m =MysqlManager.getCon();
		String SQLComment ="update "+TableName+" set "+TargetColumn+"= \""+NewValue+"\""+" where id ="+TargetId;

		try {
			m.setAutoCommit(false);
			PreparedStatement pst = m.prepareStatement(SQLComment);
			pst.executeUpdate();
			System.out.print("update command over");
			//结束后新增
			MysqlManager.FirstCommandResult =1;
			MysqlManager.DatabaseLogInput(SQLComment);
			if(MysqlManager.SecondCommandResult==1) {
				m.commit();
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		MysqlManager.close();
	    		System.out.print("Finish");
			}else {
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		System.out.print("执行失败");
	    		MysqlManager.close();
			}
    	}catch(SQLException e) {
    		e.printStackTrace();
    		MysqlManager.FirstCommandResult =0;
    		MysqlManager.SecondCommandResult =0;
    		System.out.print("执行失败");
    		MysqlManager.close();
    	}
    }

	
    public static void delete(String TableName,Integer TargetID) {
  	    Connection m =MysqlManager.getCon();
    	String SQLComment ="delete * from table "+TableName+" where id = "+TargetID;
    	
    	try {
    		PreparedStatement pst = m.prepareStatement(SQLComment);
    		m.setAutoCommit(false);
			pst.executeUpdate();
			System.out.print("delete command over");
			//结束后新增
			MysqlManager.FirstCommandResult =1;
			MysqlManager.DatabaseLogInput(SQLComment);
			if(MysqlManager.SecondCommandResult==1) {
				m.commit();
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		MysqlManager.close();
	    		System.out.print("Finish");
			}else {
	    		MysqlManager.FirstCommandResult =0;
	    		MysqlManager.SecondCommandResult =0;
	    		System.out.print("执行失败");
	    		MysqlManager.close();
			}
    	}catch(SQLException e) {
    		e.printStackTrace();
    		MysqlManager.FirstCommandResult =0;
    		MysqlManager.SecondCommandResult =0;
    		System.out.print("执行失败");
    		MysqlManager.close();
    	}
    }
    //来一个被调用的databaselog录入函数
    public static void DatabaseLogInput(String Input) {
    	Connection m =MysqlManager.getCon();
    	try {
    		m.setAutoCommit(false);
    		String SQLComment ="insert into databaselog (content,date,user)values(?,?,?)"; 
			PreparedStatement pst = m.prepareStatement(SQLComment);
			pst.setString(1, Input);
			String DateNow = DateSwitch.DateTodayWithHMS();
			pst.setString(2, DateNow);
			pst.setString(3, "李茂");
			if (MysqlManager.FirstCommandResult ==1) {
				MysqlManager.SecondCommandResult=1;
				pst.executeUpdate();
				System.out.print("log input success");
				m.commit();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MysqlManager.SecondCommandResult=0;
			System.out.print("执行二号函数失败\n");
		}
    	
    }
}
