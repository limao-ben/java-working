package SQLConnection;

public class TableHumanResource {
	Integer id;
	String name;
	String sex;
	Integer age;
	String PhoneNumber;
	String birthday;	
	
	
	public static void update(String TargetColumn,Integer TargetId,String NewValue){
		MysqlManager.update("HumanResource", TargetColumn, TargetId, NewValue);
	}
	public static void delete(Integer TargetID) {
		MysqlManager.delete("HumanResource", TargetID);
	}
	public static void add(String name,String sex,String birthday,String PhoneNumber) {
		MysqlManager.add("HumanResource", name, sex, birthday, PhoneNumber);
	}
	public static void search() {
		MysqlManager.search("HumanResource");
	}
	public static void search(Integer id) {
		MysqlManager.search("HumanResource",id);
	}
}
