package SQLConnection;

import java.util.Date;
import java.util.regex.*;
import java.text.SimpleDateFormat;

public class DateSwitch {//这个函数返回的是当日的yyyy-mm-dd
	static SimpleDateFormat DateNow = new SimpleDateFormat("yyyy-MM-dd");
	static SimpleDateFormat DateNowHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static String DateTodayWithoutHMS() {
		String T1=DateNow.format(new Date());
		//Timestamp T2 = TimeSwitch.StringToTimestamp(T1);
		return T1;
	}
	public static String DateTodayWithHMS() {
		String T1=DateNowHMS.format(new Date());
		//Timestamp T2 = TimeSwitch.StringToTimestamp(T1);
		return T1;
	}
	public static Integer CalculateAge(String DateInput) {
		//调用函数得到今天的时间
		String DateToday = DateSwitch.DateTodayWithoutHMS();
		//利用格式中的“-”进行分割，得到含有年月日三个值的数组
		Pattern p = Pattern.compile("-");
		String[] DateToday1 = p.split(DateInput);
		String[] DateToday2 = p.split(DateToday);
		//
		String year = DateToday1[0];
		String month = DateToday1[1];
		String day = DateToday1[2];
		//将三个值转换为int类型
		Integer YearInput = Integer.valueOf(year);
		Integer MonthInput = Integer.valueOf(month);
		Integer DayInput = Integer.valueOf(day);
		//给数组的各个值以定义
		String Year = DateToday2[0];
		String Month = DateToday2[1];
		String Day = DateToday2[2];
		//将三个值转换为int类型
		Integer YearNow = Integer.valueOf(Year);
		Integer MonthNow = Integer.valueOf(Month);
		Integer DayNow = Integer.valueOf(Day);
		//将生日也转换为int类型
		Integer DelterYear =0;
		Integer DelterMonth =0;
		
		if(YearNow >=YearInput) {
			DelterYear = YearNow - YearInput -1;
		}else {
			System.out.print("Input Have Mistakes");
		}
		
		if(MonthNow >MonthInput) {
			DelterMonth = 0;
		}
		
		if(MonthNow == MonthInput){
			if(DayNow >= DayInput) {
				DelterMonth = 1;
			}else {
				DelterMonth = 0;
			}
		}
		
		if(MonthNow < MonthInput) {
			DelterMonth =1;
		}
		Integer DelterResult = DelterYear + DelterMonth;
		
		
		return DelterResult;
	}
	

}
