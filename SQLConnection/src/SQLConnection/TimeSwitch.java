package SQLConnection;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimeSwitch {
	public static Timestamp StringToTimestamp(String s) {
		Timestamp T1;
		T1 = Timestamp.valueOf(s);
		return T1;
		
	}
	
	public static String TimestampToString(Timestamp t) {
		String T2;
		T2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(t);
		return T2;
	}
	
}
