package SQLConnection;

import java.sql.Timestamp;

public class TableBill {
	Integer id;
	Timestamp date;
	String name;
	Double price;
	Double quantity;
	String buyer;
	Double paying;
	public static void update(String TargetColumn,Integer TargetId,String NewValue){
		MysqlManager.update("bill", TargetColumn, TargetId, NewValue);

		}
	public static void delete(Integer TargetID) {
		MysqlManager.delete("bill", TargetID);
	}
	public static void add(String name,String date,Double price,Double quantity,String buyer) {
		MysqlManager.add("bill", date, name, price, quantity, buyer);
	}
	public static void search() {
		MysqlManager.search("bill");
	}
	public static void search(Integer id) {
		MysqlManager.search("bill",id);
	}
}
